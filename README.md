# Function Commenting Extention (jofuncce) README

This Project is an extention for Visual Studio Code.

@author Johannes Roß<br/>
@Version 1.0.0

## Features

This extention was created to better your commenting. Speciffically your function commenting. This extention might not work very good for most languages. It's been tested in CFML (cfscript) and also in Typescript. If you want me to add more languages, feel free to contact me.
## Example
Let's suppose you wrote a function in coldfusion (weird Choise, I know):

```
public array function NameOfMyFunction (required string argument1 hint="A String containing mostly letters.", 
    number integer = 0 hint="A number often not infinity.")
    hint="This function will return an array, with both given arguments. It's useless. Don't do this!" {
    return [
        arguments.argument1,
        arguments.integer
    ];
}
```

Activating this extention will generate the following command:

```
/**
 * @author NoAuthor
 * This function will return an array, with both given arguments. It's useless. Don't do this!
 *
 * [section: NoSection]
 * [category: NoCategory]
 * [scope: public]
 *
 * @argument1 !{string} A String containing mostly letters.
 * @integer {number} A number often not infinity.
 *
 * @returnValue {array} NoDescription
 */
public array function NameOfMyFunction (required string argument1 hint="A String containing mostly letters.", 
    number integer = 0 hint="A number often not infinity.")
    hint="This function will return an array, with both given arguments. It's useless. Don't do this!" {
    return [
        arguments.argument1,
        arguments.integer
    ];
}
```
## Demonstration
You can create a comment by setting your cursor in the line, where the function starts, and activating the "CCF" command.

![Typescript create comment](https://gitlab.com/johannes.ross/jojo-vsce/-/raw/master/demo/create_comment.gif)

You also can change a few default values, by calling the command CCS:

![Typescript update author](https://gitlab.com/johannes.ross/jojo-vsce/-/raw/master/demo/update_author.gif)
## Command CCS
This command opens up a few input boxes.

* The first input box asks for the type of change you want to make. You can `update` (alias `up`) or `add` a value.
* The second input box asks you for the setting you want to change. The following settings are possible to `update`:
    *  `functionDescription` the default description all comments will put out.
    *  `author` the author that wrote the functions.
    *  `section` the current section you are in.
    *  `category` the current category you are in.

* The following  settings can be added (`add`):
    *  `scopes` alias `scope` the scopes the comment will search for and put out.
    *  `types` alias `type` the types the comment will search for and put out.

* The third and last inputbox asks you for the value you want to put in. This can be any string you want. It can contain special characters or spaces or really anything.

The command saves all your input data in a file called `ccfusersettings.json` in your installation folder for your Visual Studio Code. You can edit this file all you want and how you want. Just remember that if you mess up the JSON annotation, the extention will override the broken file with it's own default values. Here is a demo that showcases how to edit the file, if you want to want to add a new type:

![Typescript update author](https://gitlab.com/johannes.ross/jojo-vsce/-/raw/master/demo/add_type_by_file.gif)

Here is the same change but, using the CCS command:

![Typescript update author](https://gitlab.com/johannes.ross/jojo-vsce/-/raw/master/demo/add_type_by_command.gif)

# Supported languages

* Typescript
* cfscript
* Javascript

## Known Issues

None yet. Feel free to contact me, if you find any.

## Release Notes 1.1.0
* Fixed a bug, where the name of a Argument was not printed our correctly if you didn't put spaces between the name and = or value.
* Added a functionality, where the function description, you can define (for example in cfml with hint="foo bar"), will actually now be displayed
* An argument which is Required will now add a ! infront of the {}
* Added a few more default types. You can either update them yourself or just delete your ccfusersettings.json

**Enjoy!**
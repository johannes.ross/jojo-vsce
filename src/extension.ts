// The module 'vscode' contains the VS Code extensibility API
import * as vscode from 'vscode';
import * as fs from 'fs';
import { Defaultvalues, CommentStructure } from './types';
import defaultvalues from './defaultvalues.json';
import { isUndefined } from 'util';
import { determineArgumentDescription, collectUserDefaultValues, determineScope, determineReturnValue, determineTabs, writeUserSettings, readUserSettings, determineFunctionDescription } from './valuecollecter';

const userSettingsFileName = 'ccfusersettings.json';

// this method is called when your extension is activated
export function activate(context: vscode.ExtensionContext) {
	let commentStruct:CommentStructure = {
		author:defaultvalues.author,
		functionDescription:defaultvalues.functionDescription,
		section:defaultvalues.section,
		category:defaultvalues.category,
		scope:defaultvalues.scopes[0],
		type:defaultvalues.types[0],
		argsOutput: [
			{
				name:"",
				type:"",
				description:"",
				required:false
			}
		],
		return: {
			has: defaultvalues.returnValue.has,
			type: defaultvalues.returnValue.type,
			description: defaultvalues.returnValue.description
		}
	};
	if(fs.existsSync(userSettingsFileName)){
		commentStruct = readUserSettings(userSettingsFileName, commentStruct);
	}else{
		writeUserSettings(userSettingsFileName, commentStruct);
	}

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let completeComment = vscode.commands.registerCommand('jofuncce.CommentCompleteFunction', () => {
		let feedbackMessage:string = 'Nothing happened. You probably don\'t have a file open!';
		let activeTextEditor:vscode.TextEditor|undefined = vscode.window.activeTextEditor;
		if(activeTextEditor !== undefined){
			try{
				let functionToComment = activeTextEditor.document.lineAt(new vscode.Position (activeTextEditor.selection.start.line, 0)).text;
				let correctSyntax = functionToComment.indexOf("(") >= 0;
				if(correctSyntax){
					correctSyntax = functionToComment.indexOf(")") > 0;
					if(!correctSyntax){
						let curLine = activeTextEditor.selection.start;
						while(!correctSyntax){
							curLine = new vscode.Position(curLine.line + 1, 0);
							if(activeTextEditor.document.lineCount >= curLine.line){
								functionToComment += activeTextEditor.document.lineAt(curLine).text;
								correctSyntax = functionToComment.indexOf(")") > 0;
							}else{
								feedbackMessage = "You don't have a ) in your function!";
								break;
							}
						}
					}
					correctSyntax = functionToComment.indexOf("{") > 0;
					if(!correctSyntax){
						let curLine = activeTextEditor.selection.start;
						while(!correctSyntax){
							curLine = new vscode.Position(curLine.line + 1, 0);
							if(activeTextEditor.document.lineCount >= curLine.line){
								functionToComment += activeTextEditor.document.lineAt(curLine).text;
								correctSyntax = functionToComment.indexOf(")") > 0;
							}else{
								feedbackMessage = "You don't have a { in your function!";
								break;
							}
						}
					}
				}else{
					feedbackMessage = "You do not seem to be in the correct line. Please make sure, that you have (!";
				}
				if(correctSyntax){
					let workCopy:string = functionToComment;
					let userSettings:Defaultvalues = collectUserDefaultValues(userSettingsFileName);
					commentStruct = readUserSettings(userSettingsFileName, commentStruct);
					commentStruct.argsOutput = determineArgumentDescription(workCopy, userSettings.types);
					commentStruct.scope = determineScope(workCopy, userSettings.scopes);
					commentStruct.return = determineReturnValue(workCopy, userSettings.types, userSettings.returnValue);
					commentStruct.functionDescription = determineFunctionDescription(workCopy, commentStruct.functionDescription);
					activeTextEditor.insertSnippet(
						new vscode.SnippetString(assambleBlockComment(commentStruct, determineTabs(activeTextEditor))),
						new vscode.Position (activeTextEditor.selection.anchor.line,0)
					);
					feedbackMessage = 'Successully added function comment.';
				}
			}catch(error){
				console.log(error);
				feedbackMessage = 'Whoops! Something went wrong! Please make sure, that your Syntax is correct!'; 
			}
		}
		// Display a message box to the user
		vscode.window.showInformationMessage(feedbackMessage);
	});
	let changeCurrentSetting = vscode.commands.registerCommand('jofuncce.ChangeCommentSettings', () =>{
		vscode.window.showInputBox({prompt:"Please enter the Type of Change you want to make."}).then((typeOfChange)=>{
			if(isUndefined(typeOfChange) || typeOfChange.length === 0){
				vscode.window.showErrorMessage('Please enter after the command a type of change: values: add, update\n' +
				'a setting, and a newValue');
				return;
			}
			vscode.window.showInputBox({prompt:"Please enter the setting you want to change."}).then((setting)=>{
				if(isUndefined(setting) || setting.length === 0){
					vscode.window.showErrorMessage('Please enter after the command a type of change: values: add, update\n' +
					'a setting, and a newValue');
					return;
				}
				vscode.window.showInputBox({prompt:"Please enter the new value for your change."}).then((newValue)=>{
					if(isUndefined(newValue) || newValue.length === 0){
						vscode.window.showErrorMessage('Please enter after the command a type of change: values: add, update\n' +
						'a setting, and a newValue');
						return;
					}
					commentStruct = changeSetting(typeOfChange, setting, newValue, commentStruct);
				});
			});
		});
	});
	context.subscriptions.push(completeComment);
	context.subscriptions.push(changeCurrentSetting);
}

/**
 * @author Johannes Roß
 * This function changes one setting. If wanted it can also save the default.
 * 
 * [section: Main Extention]
 * [category: Saving]
 * [scope: private]
 * 
 * @typeOfChange {string} the type of change you want to make. Possible values are: "add", "update"
 * @setting {string} the name of the setting you want to change.
 * @newValue {string} the new value you want to change the setting to.
 * @commentStruct {CommentStructure} the current settings
 * @saveDefault {boolean} is the value you are changing a default value, that should be changed.
 * 
 * @returns {CommentStructure} The new saved settings
 * 
 */
function changeSetting(typeOfChange:string, setting:string, newValue:string, commentStruct:CommentStructure):CommentStructure{
	let madeChange = false;
	switch(typeOfChange){
		case "add":
			madeChange = true;
			switch(setting){
				case "type":
				case "types":
					commentStruct.type = newValue;
					break;
				case "scope":
				case "scopes":
					commentStruct.scope = newValue;
					break;
				default:
					vscode.window.showErrorMessage('You cannot add that setting.');
					madeChange = false;
					break;
			}
			break;
		case "up":
		case "update":
			madeChange = true;
			switch(setting){
				case "author":
					commentStruct.author = newValue;
					break;
				case "category":
					commentStruct.category = newValue;
					break;
				case "section":
					commentStruct.section = newValue;
					break;
				case "functionDescription":
					commentStruct.functionDescription = newValue;
					break;
				case "scope":
					commentStruct.scope = newValue;
					break;
				case "type":
					commentStruct.type = newValue;
				default:
					vscode.window.showErrorMessage('There is no such setting.');
					madeChange = false;
					break;
			}
			break;
		default:
			vscode.window.showErrorMessage('That change is not possible.');
			madeChange = false;
			break;
	}
	if(madeChange){
		writeUserSettings(userSettingsFileName , commentStruct);
	}
	return commentStruct;
}
/**
 * @author Johannes Roß
 * This function creates a String, which is marked up.
 * 
 * [section: Main Extention]
 * [category: Assamble]
 * [scope: private]
 * 
 * @collectedInfo {CommentStructure} struct containing all needed Infos
 * @timesInlined {number} times the function is tabbed (4 spaces = 1 Tab)
 * 
 * @returns {string} A Comment containing the info given
 * 
 */
function assambleBlockComment(collectedInfo:CommentStructure, timesInlined:number):string{
	let assambledComment:string = "";
	timesInlined = timesInlined > 0 ? timesInlined : 0;
	assambledComment += "\t".repeat(timesInlined) + "/**\n";
	assambledComment += "\t".repeat(timesInlined) + " * @author " + collectedInfo.author + "\n";
	assambledComment += "\t".repeat(timesInlined) + " * " + collectedInfo.functionDescription + "\n";
	assambledComment += "\t".repeat(timesInlined) + " * \n";
	assambledComment += "\t".repeat(timesInlined) + " * [section: " + collectedInfo.section + "]\n";
	assambledComment += "\t".repeat(timesInlined) + " * [category: " + collectedInfo.category + "]\n";
	assambledComment += "\t".repeat(timesInlined) + " * [scope: " + collectedInfo.scope + "]\n";
	assambledComment += "\t".repeat(timesInlined) + " * \n";
	collectedInfo.argsOutput.forEach((arg) => {
		if(arg.name.length > 0){
			assambledComment += "\t".repeat(timesInlined) + " * @" + arg.name + " " + (arg.required ? "!" : "") + "{" + arg.type + "} " + arg.description + "\n";
		}
	})
	if(collectedInfo.return.has){
		assambledComment += "\t".repeat(timesInlined) + " * \n";
		assambledComment += "\t".repeat(timesInlined) + " * @returns {" + collectedInfo.return.type + "} " + collectedInfo.return.description + "\n";
	}
	assambledComment += "\t".repeat(timesInlined) + " */\n";
	return assambledComment;
}

// this method is called when your extension is deactivated
export function deactivate() {}

import { Defaultvalues, ArgumentDescription, ReturnValue, CommentStructure} from './types';
import defaultvalues from './defaultvalues.json';
import * as fs from 'fs';
import * as vscode from 'vscode';
/**
 * @author Johannes Roß
 * This function returns a scope, which it found in the given string
 * 
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: public]
 * 
 * @searchThrough {string} a string in which to find the scope
 * @scopes {string[]} the scopes which are avaiable
 * 
 * @returns {string} The determined scope or the first scope in the scopes[]
 * 
 */
export function determineScope(searchThrough:string, scopes:string[]):string{
    let scope:string = findWordOfArrayInString(searchThrough, scopes);
    if(scopes.length > 0){
        if(scope.length === 0){
            scope = scopes[0];
        }
    }else{
        scope = "Error please define scopes!";
    }
    return scope;
}
/**
 * @author Johannes Roß
 * This function returns a type, which it found in the given string
 * 
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: public]
 * 
 * @searchThrough {string} a string in which to find the type
 * @types {string[]} the types which are avaiable
 * 
 * @returns {string} The determined type or the first type in the types[]
 * 
 */
export function determineTypes(searchThrough:string, types:string[]):string{
    let type:string = findWordOfArrayInString(searchThrough, types);
    if(types.length > 0){
        if(type.length === 0){
            type = types[0];
        }
    }else{
        type = "Error please define types!";
    }
    return type;
}
/**
 * @author Johannes Roß
 * This function returns a return, which it found in the given string
 * 
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: public]
 * 
 * @searchThrough {string} a string in which to find the type
 * @types {string[]} the types which are avaiable
 * 
 * @returns {ReturnValue} A Struct containing all the Infos, it could find
 * 
 */
export function determineReturnValue(searchThrough:string, types:string[], returnDefaultSettings:ReturnValue):ReturnValue{
    let curReturnValue:ReturnValue = {
        has: returnDefaultSettings.has,
        type: returnDefaultSettings.type,
        description: returnDefaultSettings.description
    }
    let infrontOfBrackets:string = searchThrough.substring(0, searchThrough.indexOf("(")).trim();
    let behindBrackets:string = searchThrough.substring( searchThrough.indexOf(")") + 1, searchThrough.indexOf("{")).trim();
    curReturnValue.type = findWordOfArrayInString(infrontOfBrackets, types);
    if(curReturnValue.type.length === 0){
        curReturnValue.type = findWordOfArrayInString(behindBrackets, types);
        if(curReturnValue.type.length > 0){
            curReturnValue.has = true;
        }else{
            curReturnValue.has = false;
        }
    }else{
        curReturnValue.has = true;
    }
    return curReturnValue;
}
/**
 * @author Johannes Roß
 * This function returns an ArgumentDescription[]
 * 
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: public]
 * 
 * @searchThrough {string} a string in which to find arguments
 * @types {string[]} the types which are avaiable
 * 
 * @returns {ArgumentDescription[]} All arguments found in this string
 * 
 */
export function determineArgumentDescription(searchThrough:string, types:string[]):ArgumentDescription[]{
    let allArguments:ArgumentDescription[] = [{
        name:"",
        description:"",
        type:"",
        required:false
    }];
    let stringWithinFunctionBrackets:string = "";
    if(searchThrough.indexOf("(") > -1 && searchThrough.indexOf(")") > -1){
        stringWithinFunctionBrackets = searchThrough.substring(searchThrough.indexOf("(") + 1, searchThrough.indexOf(")"));
        let allArgumentsInString:string[] = stringWithinFunctionBrackets.split(",");
        let curArrayPosition:number = 0;
        allArgumentsInString.map( (curArgument) => {
            curArgument = curArgument.trim();
            let newArgument:ArgumentDescription = {
                name: "",
                description: "",
                type: findWordOfArrayInString(curArgument, types),
                required: (curArgument.indexOf("required") !== -1)
            };
            let nameStart:number = newArgument.required ? "required ".length : 0;
            let searchChar = " ";
            if(curArgument.indexOf(":") !== -1){
                if(curArgument.indexOf("?:") !== -1){
                    newArgument.required = false;
                    searchChar = "?"
                }else{
                    newArgument.required = true;
                    searchChar = ":";
                }
            }else if(curArgument.indexOf(searchChar) === -1){
                searchChar = "DoNotFindThis";
            }
            if(newArgument.type.length > 0 && searchChar === " "){
                nameStart += newArgument.type.length + 1;
                if(curArgument.indexOf("=", nameStart) !== -1
                    && (curArgument.indexOf(searchChar, nameStart) !== -1 && curArgument.indexOf(searchChar, nameStart) > curArgument.indexOf("=", nameStart))
                    || curArgument.indexOf(searchChar, nameStart) === -1){
                    searchChar = "=";
                }
            }
            let nameEnd = curArgument.indexOf(searchChar, nameStart);
            if(searchChar === "DoNotFindThis"){
                nameEnd = curArgument.length;
                newArgument.required = true;
            }else if(nameEnd === -1){
                nameEnd = curArgument.length;
            }
            newArgument.name = curArgument.substring(nameStart, nameEnd);
            let searchForDescription:boolean = (curArgument.indexOf("'") > -1 || curArgument.indexOf('"') > -1);
            if(searchForDescription){
                let searchQuates = curArgument.indexOf("'") > 0 ? "'" : '"';
                newArgument.description = curArgument.substring(curArgument.indexOf(searchQuates) + 1, 
                    curArgument.indexOf(searchQuates, curArgument.lastIndexOf(searchQuates)));
            }
            if(allArguments.length == curArrayPosition + 1
                && allArguments[curArrayPosition].name.length
                + allArguments[curArrayPosition].description.length
                + allArguments[curArrayPosition].type.length === 0 
                && allArguments[curArrayPosition].required === false
            ){
                allArguments[curArrayPosition] = newArgument;
                curArrayPosition++;
            }else{
                allArguments.push(newArgument);
            }
        });
    }
    return allArguments;
}
/**
 * @author Johannes Roß
 * This function returns a scope, which it found in the given string
 * 
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: private]
 * 
 * @searchThrough {string} a string in which to find the scope
 * @arrayWithValues {string[]} the scopes which are avaiable
 * 
 * @returns {string} The determined scope or an empty string
 * 
 */
function findWordOfArrayInString(searchThrough:string,arrayWithValues:string[]):string{
    let determined:string = "";
    if(arrayWithValues.length > 0){
        for(let value of arrayWithValues){
            let found:boolean = false;
            let indexOfItem:number = searchThrough.indexOf(value);
            if(indexOfItem > -1){
                let frontFound:boolean = (indexOfItem === 0);
                if(!frontFound){
                    if(searchThrough.charAt(indexOfItem - 1) === " "
                        || searchThrough.charAt(indexOfItem - 1) === ":"){
                        frontFound = true;
                    }
                }
                if(frontFound){
                    let behindFound:boolean = (indexOfItem + value.length === searchThrough.length);
                    if(!behindFound){
                        if(searchThrough.charAt(indexOfItem + value.length) === " "
                            || searchThrough.charAt(indexOfItem + value.length) === "{"
                            || searchThrough.charAt(indexOfItem + value.length) === "["){
                            behindFound = true;
                        }
                    }
                    found = behindFound && frontFound;
                }
                if(found){
                    determined = value;
                    break;
                }
            }
        }
    }
    return determined;
}
/**
 * @author Johannes Roß
 * This function returns all UserSet Default Values or the default values
 * 
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: public]
 * 
 * @searchThrough {string} a string in which to find the type
 * 
 * @returns {string} The determined type or the first type in the types[]
 * 
 */
export function collectUserDefaultValues(userSettingsFileName:string):Defaultvalues{
    let updatedDefaultSettings:Defaultvalues = defaultvalues;
    if(fs.existsSync(userSettingsFileName)){
        let fileContent:string = fs.readFileSync(userSettingsFileName, {encoding: 'UTF8'});
        try{
            updatedDefaultSettings = JSON.parse(fileContent);
        }catch(syntax){
            updatedDefaultSettings = defaultvalues;
            vscode.window.showErrorMessage('Your JSON file seemed to have a Syntax Error in it. If you don\'nt fix it, it will be overwritten. Syntax Error: ' + syntax.message);
        }
    }
    return updatedDefaultSettings;
}
/**
 * @author Johannes Roß
 * This function calculates a integervalue of the current position of the selection
 * 
 * [section: Value Collecter]
 * [category: Calculation]
 * [scope: private]
 * 
 * @currentTexteditor {TextEditor} a Texteditor where a selection is made
 * 
 * @returns {number} An integer with a tabcount
 * 
 */
export function determineTabs(currentTexteditor:vscode.TextEditor):number{
	let tabCount:number = 0;
	try {
		let whitespaceCount:number = currentTexteditor.document.lineAt(currentTexteditor.selection.anchor.line).firstNonWhitespaceCharacterIndex;
		tabCount = Math.round((whitespaceCount / 4));
	} catch (error) {
		tabCount = 0;
	}	
	return tabCount;
}
/**
 * @author Johannes Roß
 * This function saves the current settings in a json file
 * 
 * [section: Value Collecter]
 * [category: File Management]
 * [scope: public]
 * 
 * @userSettingsFileName {string} the name of the file
 * @commentStruct {CommentStructure} the current settings
 * 
 * @returns {CommentStructure} The new saved settings
 * 
 */
export function writeUserSettings(userSettingsFileName:string, commentStruct:CommentStructure):CommentStructure {
	let updatedDefaultSettings:Defaultvalues = collectUserDefaultValues(userSettingsFileName);
	// Update settings
	if(!updatedDefaultSettings.scopes.includes(commentStruct.scope, 0)){
		updatedDefaultSettings.scopes.push(commentStruct.scope);
	}
	if(!updatedDefaultSettings.types.includes(commentStruct.type, 0)){
		updatedDefaultSettings.types.push(commentStruct.type);
	}
	updatedDefaultSettings.author = commentStruct.author;
	updatedDefaultSettings.section = commentStruct.section;
	updatedDefaultSettings.category = commentStruct.category;
	updatedDefaultSettings.functionDescription = commentStruct.functionDescription;
    fs.writeFile(userSettingsFileName, JSON.stringify(updatedDefaultSettings),  function(err:any) {
        if (err) {
            vscode.window.showErrorMessage('Whoops something went wrong: ' + err);
        }else{
            vscode.window.showInformationMessage('Settings saved in: ' + userSettingsFileName);
        }
    });
	return commentStruct;
}
/**
 * @author Johannes Roß
 * This function reads current settings from a json file if it already exists
 * 
 * [section: Value Collecter]
 * [category: File Management]
 * [scope: public]
 * 
 * @userSettingsFileName {string} the name of the file
 * @commentStruct {CommentStructure} the current settings
 * 
 * @returns {CommentStructure} The new saved settings
 * 
 */
export function readUserSettings(userSettingsFileName:string, commentStruct:CommentStructure):CommentStructure {
	const fs = require('fs');
	if(fs.existsSync(userSettingsFileName)){
        let updatedDefaultSettings:Defaultvalues = collectUserDefaultValues(userSettingsFileName);
        // Update settings
        commentStruct.author = updatedDefaultSettings.author;
        commentStruct.category = updatedDefaultSettings.category;
        commentStruct.functionDescription = updatedDefaultSettings.functionDescription;
        commentStruct.section = updatedDefaultSettings.section;
        commentStruct.return.has = updatedDefaultSettings.returnValue.has;
        commentStruct.return.description = updatedDefaultSettings.returnValue.description;
        commentStruct.return.type = updatedDefaultSettings.returnValue.type;
    }
	return commentStruct;
}
/**
 * @author Johannes Roß
 * This function searches for a Function Description and returns it, if it finds it
 * 
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: export]
 * 
 * @searchThrough {string} The string containing the description
 * @defaultDescription {string} The description you want to set, if no Description is found
 * 
 * @returns {string} The found description or your set value
 */
export function determineFunctionDescription(searchThrough:string, defaultDescription:string):string{
    let functionDescription = defaultDescription;
    if(searchThrough.indexOf(")") > -1 && searchThrough.indexOf("{") > 0){
        let behindBrackets:string = searchThrough.substring(searchThrough.indexOf(")") + 1, searchThrough.indexOf("{"));
        let searchChar:string = "\"";
        let found:boolean = false;
        if(behindBrackets.indexOf(searchChar) > -1 && behindBrackets.indexOf(searchChar) != behindBrackets.lastIndexOf(searchChar)){
            found = true;
        }else if(behindBrackets.indexOf("'") > -1 && behindBrackets.indexOf("'") != behindBrackets.lastIndexOf("'")){
            searchChar = "'";
            found = true;
        }
        if(found){
            functionDescription = behindBrackets.substring(behindBrackets.indexOf(searchChar) + 1, behindBrackets.lastIndexOf(searchChar))
        }
    }
    return functionDescription;
}
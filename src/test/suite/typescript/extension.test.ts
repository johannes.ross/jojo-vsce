import * as assert from 'assert';
import { determineTypes, determineScope, determineReturnValue, determineArgumentDescription } from '../../../valuecollecter';
import * as defaultValues from '../../../defaultvalues.json';

suite('typescript', () => {
	suite('Value Collecter', () =>{
		suite('Determine', async () => {
			test('Type correct values', async () => {
				let type = 'searchThrough:number';
				assert.equal(defaultValues.types[1], determineTypes(type, defaultValues.types))
			});
			test('Type incorrect input', async () => {
				let type = 'private I don\'t know ';
				assert.equal(defaultValues.types[0], determineTypes(type, defaultValues.types))
			});
			test('Type incorrect types', async () => {
				let type = 'searchThrough:number';
				assert.equal("Error please define types!", determineTypes(type, []))
			});
			test('Scope correct values', async () => {
				let scope = 'export function determineScope(searchThrough:string, scopes:string[]):string{';
				assert.equal(defaultValues.scopes[5], determineScope(scope, defaultValues.scopes))
			});
			test('Scope incorrect input', async () => {
				let scope = 'prive I don\'t know ';
				assert.equal(defaultValues.scopes[0], determineScope(scope, defaultValues.scopes))
			});
			test('Scope incorrect scopes', async () => {
				let scope = 'export function determineScope(searchThrough:string, scopes:string[]):string{';
				assert.equal("Error please define scopes!", determineScope(scope, []))
			});
			test('ReturnValue correct input behind of brackets', async () => {
				let returnValue = 'export function determineScope(searchThrough:string, scopes:string[]):number{';
				assert.deepEqual({
					has: true,
					type: defaultValues.types[1],
					description: defaultValues.returnValue.description
				}, determineReturnValue(returnValue, defaultValues.types, defaultValues.returnValue));
			});
			test('ReturnValue false input no brackets', async () => {
				let returnValue = 'private fun:number';
				assert.deepEqual({
					has: false,
					type: '',
					description: defaultValues.returnValue.description
				}, determineReturnValue(returnValue, defaultValues.types, defaultValues.returnValue));
			});
			test('ReturnValue false types', async () => {
				let returnValue = 'export function determineScope(searchThrough:string, scopes:string[]):string{';
				assert.deepEqual({
					has: false,
					type: '',
					description: defaultValues.returnValue.description
				}, determineReturnValue(returnValue, [], defaultValues.returnValue));
			});
			test('ReturnValue correct input diffrent default Values', async () => {
				let returnValue = '():{';
				assert.deepEqual({
					has: false,
					type: "",
					description: "Test Diffrent Standard"
				}, determineReturnValue(returnValue, defaultValues.types, {
					has: true,
					type: defaultValues.types[3],
					description: "Test Diffrent Standard"
				}));
			});
			test('ReturnValue false types', async () => {
				let returnValue = 'export function determineScope(searchThrough:string, scopes:string[]):string{';
				assert.deepEqual({
					has: false,
					type: '',
					description: defaultValues.returnValue.description
				}, determineReturnValue(returnValue, [], defaultValues.returnValue));
			});
			test('ArgumentDescription correct input', async () => {
				let argumentDescription = 'export function determineScope(searchThrough:number):string{';
				assert.deepEqual([{
					name: "searchThrough",
					type: defaultValues.types[1],
					required: true,
					description: ""
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription incorrect input no brackets', async () => {
				let argumentDescription = 'export function determineScope searchThrough:number :string{';
				assert.deepEqual([{
					name: "",
					type: "",
					required: false,
					description: ""
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription correct input not required', async () => {
				let argumentDescription = 'export function determineScope(searchThrough?:number):string{';
				assert.deepEqual([{
					name: "searchThrough",
					type: defaultValues.types[1],
					required: false,
					description: ""
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription incorrect input no type', async () => {
				let argumentDescription = 'export function determineScope(searchThrough):string{';
				assert.deepEqual([{
					name: "searchThrough",
					type: "",
					required: true,
					description: ""
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription incorrect input no name', async () => {
				let argumentDescription = 'export function determineScope(number):string{';
				assert.deepEqual([{
					name: defaultValues.types[1],
					type: defaultValues.types[1],
					required: true,
					description: ""
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription false types', async () => {
				let argumentDescription = 'export function determineScope(number):string{';
				assert.deepEqual([{
					name: "number",
					type: "",
					required: true,
					description: ""
				}], determineArgumentDescription(argumentDescription, []));
			});
			test('ArgumentDescription correct input more than 1 argument', async () => {
				let argumentDescription = 'export number determineScope(searchThrough:number, scopes?:decimal[]):string{';
				assert.deepEqual([
					{
						name: "searchThrough",
						type: defaultValues.types[1],
						required: true,
						description: ""
					},
					{
						name: "scopes",
						type: defaultValues.types[2],
						required: false,
						description: ""
					}
				], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
		});
	});
});

import * as assert from 'assert';
import { determineTypes, determineScope, determineReturnValue, determineArgumentDescription, determineFunctionDescription } from '../../../valuecollecter';
import * as defaultValues from '../../../defaultvalues.json';

suite('cfml', () => {
	suite('Value Collecter', () =>{
		suite('Determine', async () => {
			test('Type correct values', async () => {
				let type = 'private number I don\'t know ';
				assert.equal(defaultValues.types[1], determineTypes(type, defaultValues.types))
			});
			test('Type incorrect input', async () => {
				let type = 'private I don\'t know ';
				assert.equal(defaultValues.types[0], determineTypes(type, defaultValues.types))
			});
			test('Type incorrect types', async () => {
				let type = 'private number I don\'t know ';
				assert.equal("Error please define types!", determineTypes(type, []))
			});
			test('Scope correct values', async () => {
				let scope = 'private number I don\'t know ';
				assert.equal(defaultValues.scopes[1], determineScope(scope, defaultValues.scopes))
			});
			test('Scope incorrect input', async () => {
				let scope = 'prive I don\'t know ';
				assert.equal(defaultValues.scopes[0], determineScope(scope, defaultValues.scopes))
			});
			test('Scope incorrect scopes', async () => {
				let scope = 'private number I don\'t know ';
				assert.equal("Error please define scopes!", determineScope(scope, []))
			});
			test('ReturnValue correct input infront of brackets', async () => {
				let returnValue = 'private number fun(){';
				assert.deepEqual({
					has: true,
					type: defaultValues.types[1],
					description: defaultValues.returnValue.description
				}, determineReturnValue(returnValue, defaultValues.types, defaultValues.returnValue));
			});
			test('ReturnValue correct input behind of brackets', async () => {
				let returnValue = 'private fun():number {';
				assert.deepEqual({
					has: true,
					type: defaultValues.types[1],
					description: defaultValues.returnValue.description
				}, determineReturnValue(returnValue, defaultValues.types, defaultValues.returnValue));
			});
			test('ReturnValue false input no brackets', async () => {
				let returnValue = 'private fun:number';
				assert.deepEqual({
					has: false,
					type: '',
					description: defaultValues.returnValue.description
				}, determineReturnValue(returnValue, defaultValues.types, defaultValues.returnValue));
			});
			test('ReturnValue false types', async () => {
				let returnValue = 'private fun():number {';
				assert.deepEqual({
					has: false,
					type: '',
					description: defaultValues.returnValue.description
				}, determineReturnValue(returnValue, [], defaultValues.returnValue));
			});
			test('ReturnValue correct input diffrent default Values', async () => {
				let returnValue = '():{';
				assert.deepEqual({
					has: false,
					type: "",
					description: "Test Diffrent Standard"
				}, determineReturnValue(returnValue, defaultValues.types, {
					has: true,
					type: defaultValues.types[3],
					description: "Test Diffrent Standard"
				}));
			});
			test('ReturnValue false types', async () => {
				let returnValue = 'private fun():number {';
				assert.deepEqual({
					has: false,
					type: '',
					description: defaultValues.returnValue.description
				}, determineReturnValue(returnValue, [], defaultValues.returnValue));
			});
			test('ArgumentDescription correct input', async () => {
				let argumentDescription = '(required number test = 123 hint "Test Description")';
				assert.deepEqual([{
					name: "test",
					type: defaultValues.types[1],
					required: true,
					description: "Test Description"
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription incorrect input no brackets', async () => {
				let argumentDescription = 'required number test = 123 hint "Test Description"';
				assert.deepEqual([{
					name: "",
					type: "",
					required: false,
					description: ""
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription correct input not required', async () => {
				let argumentDescription = '(number test = 123 hint "Test Description")';
				assert.deepEqual([{
					name: "test",
					type: defaultValues.types[1],
					required: false,
					description: "Test Description"
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription correct input not required but name is at the end', async () => {
				let argumentDescription = '(number test)';
				assert.deepEqual([{
					name: "test",
					type: defaultValues.types[1],
					required: false,
					description: ""
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription correct input no type', async () => {
				let argumentDescription = '(required test = 123 hint "Test Description")';
				assert.deepEqual([{
					name: "test",
					type: "",
					required: true,
					description: "Test Description"
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription incorrect input no name', async () => {
				let argumentDescription = '(required number = 123 hint "Test Description")';
				assert.deepEqual([{
					name: "",
					type: defaultValues.types[1],
					required: true,
					description: "Test Description"
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription incorrect input no description', async () => {
				let argumentDescription = '(required number test = 123 hint )';
				assert.deepEqual([{
					name: "test",
					type: defaultValues.types[1],
					required: true,
					description: ""
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription false types', async () => {
				let argumentDescription = '(required number test = 123 hint "Test Description")';
				assert.deepEqual([{
					name: "number",
					type: "",
					required: true,
					description: "Test Description"
				}], determineArgumentDescription(argumentDescription, []));
			});
			test('ArgumentDescription correct input more than 1 argument', async () => {
				let argumentDescription = '(required number test = 123 hint "Test Description", decimal coolName = 0.0 \'cats\')';
				assert.deepEqual([
					{
						name: "test",
						type: defaultValues.types[1],
						required: true,
						description: "Test Description"
					},
					{
						name: "coolName",
						type: defaultValues.types[2],
						required: false,
						description: "cats"
					}
				], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription correct input', async () => {
				let argumentDescription = 'private query function getMotorenByFgr(required string fgr_id) hint="returns all motors"{';
				assert.deepEqual([
					{
						name: "fgr_id",
						type: defaultValues.types[0],
						required: true,
						description: ""
					}
				], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription correct input', async () => {
				let argumentDescription = 'private query function getMotorenByFgr(boolean render=true) hint="returns all motors"{';
				assert.deepEqual([
					{
						name: "render",
						type: "boolean",
						required: false,
						description: ""
					}
				], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('FunctionDescription correct input \"', async () => {
				let functionDescription = 'private query function getMotorenByFgr(required string fgr_id) hint="returns all motors"{';
				assert.equal("returns all motors", determineFunctionDescription(functionDescription, defaultValues.functionDescription));
			});
			test('FunctionDescription correct input \'', async () => {
				let functionDescription = 'private query function getMotorenByFgr(required string fgr_id) hint=\'returns all motors\'{';
				assert.equal("returns all motors", determineFunctionDescription(functionDescription, defaultValues.functionDescription));
			});
			test('FunctionDescription correct input no description', async () => {
				let functionDescription = 'private query function getMotorenByFgr(required string fgr_id) hint={';
				assert.equal(defaultValues.functionDescription, determineFunctionDescription(functionDescription, defaultValues.functionDescription));
			});
			test('FunctionDescription false input no \' or "', async () => {
				let functionDescription = 'private query function getMotorenByFgr(required string fgr_id) hint=returns all motors{';
				assert.equal(defaultValues.functionDescription, determineFunctionDescription(functionDescription, defaultValues.functionDescription));
			});
			test('FunctionDescription false input no \' or "', async () => {
				let functionDescription = 'private query function getMotorenByFgr(required string fgr_id) hint=returns all motors{';
				assert.equal(defaultValues.functionDescription, determineFunctionDescription(functionDescription, defaultValues.functionDescription));
			});
			test('FunctionDescription false input one \'', async () => {
				let functionDescription = 'private query function getMotorenByFgr(required string fgr_id) hint=\"returns all motors{';
				assert.equal(defaultValues.functionDescription, determineFunctionDescription(functionDescription, defaultValues.functionDescription));
			});
			test('FunctionDescription false input one \"', async () => {
				let functionDescription = 'private query function getMotorenByFgr(required string fgr_id) hint=\"returns all motors{';
				assert.equal(defaultValues.functionDescription, determineFunctionDescription(functionDescription, defaultValues.functionDescription));
			});
		});
	});
});

import * as assert from 'assert';
import { determineTypes, determineScope, determineReturnValue, determineArgumentDescription } from '../../../valuecollecter';
import * as defaultValues from '../../../defaultvalues.json';

suite('javascript', () => {
	suite('Value Collecter', () =>{
		suite('Determine', async () => {
			test('Type correct values', async () => {
				let type = 'function name(parameter1, parameter2, parameter3) {';
				assert.equal(defaultValues.types[0], determineTypes(type, defaultValues.types))
			});
			test('Scope correct values', async () => {
				let scope = 'function name(parameter1, parameter2, parameter3) {';
				assert.equal(defaultValues.scopes[0], determineScope(scope, defaultValues.scopes))
			});
			test('ReturnValue correct input behind of brackets', async () => {
				let returnValue = 'function name(parameter1, parameter2, parameter3) {';
				assert.deepEqual({
					has: false,
					type: "",
					description: defaultValues.returnValue.description
				}, determineReturnValue(returnValue, defaultValues.types, defaultValues.returnValue));
			});
			test('ArgumentDescription correct input', async () => {
				let argumentDescription = 'function name(parameter1) {';
				assert.deepEqual([{
					name: "parameter1",
					type: "",
					required: true,
					description: ""
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription incorrect input no brackets', async () => {
				let argumentDescription = 'function name parameter1 {';
				assert.deepEqual([{
					name: "",
					type: "",
					required: false,
					description: ""
				}], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
			test('ArgumentDescription correct input more than 1 argument', async () => {
				let argumentDescription = 'function name(parameter1, parameter2) {';
				assert.deepEqual([
					{
						name: "parameter1",
						type: "",
						required: true,
						description: ""
					},
					{
						name: "parameter2",
						type: "",
						required: true,
						description: ""
					}
				], determineArgumentDescription(argumentDescription, defaultValues.types));
			});
		});
	});
});

export type Defaultvalues = {
    scopes:string[], 
    returnValue:{
        has:boolean,
        type:string,
        description:string
    },
    types:string[],
    functionDescription:string,
    author:string,
    section:string,
    category:string
}

export type ArgumentDescription = {
    name:string,
    type:string,
    description:string,
    required:boolean
}

export type CommentStructure = {
    author:string,
    functionDescription:string,
    section:string,
    category:string
    scope:string,
    type:string,
    argsOutput:ArgumentDescription[],
    return: ReturnValue
}

export type ReturnValue = {
    has:boolean,
    type:string,
    description:string
}

export default Defaultvalues;
# Change Log

All notable changes to the "jofuncce" extension will be documented in this file.

## [Unreleased]

- Initial release

### 1.0.0

Initial release of this thing.

### 1.0.1
Corrected an error in the Readme File.

### 1.0.2
It Supports now Javascipt.

Added Javascipt tests, which do work.

### 1.1.0
* Fixed a bug, where the name of a Argument was not printed our correctly if you didn't put spaces between the name and = or value
* Added a functionality, where the function description, you can define (for example in cfml with hint="foo bar"), will actually now be displayed
* An argument which is Required will now add a ! infront of the {}
* Added a few more default types. You can either update them yourself or just delete your ccfusersettings.json
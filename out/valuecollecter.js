"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const defaultvalues_json_1 = __importDefault(require("./defaultvalues.json"));
const fs = __importStar(require("fs"));
const vscode = __importStar(require("vscode"));
/**
 * @author Johannes Roß
 * This function returns a scope, which it found in the given string
 *
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: public]
 *
 * @searchThrough {string} a string in which to find the scope
 * @scopes {string[]} the scopes which are avaiable
 *
 * @returns {string} The determined scope or the first scope in the scopes[]
 *
 */
function determineScope(searchThrough, scopes) {
    let scope = findWordOfArrayInString(searchThrough, scopes);
    if (scopes.length > 0) {
        if (scope.length === 0) {
            scope = scopes[0];
        }
    }
    else {
        scope = "Error please define scopes!";
    }
    return scope;
}
exports.determineScope = determineScope;
/**
 * @author Johannes Roß
 * This function returns a type, which it found in the given string
 *
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: public]
 *
 * @searchThrough {string} a string in which to find the type
 * @types {string[]} the types which are avaiable
 *
 * @returns {string} The determined type or the first type in the types[]
 *
 */
function determineTypes(searchThrough, types) {
    let type = findWordOfArrayInString(searchThrough, types);
    if (types.length > 0) {
        if (type.length === 0) {
            type = types[0];
        }
    }
    else {
        type = "Error please define types!";
    }
    return type;
}
exports.determineTypes = determineTypes;
/**
 * @author Johannes Roß
 * This function returns a return, which it found in the given string
 *
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: public]
 *
 * @searchThrough {string} a string in which to find the type
 * @types {string[]} the types which are avaiable
 *
 * @returns {ReturnValue} A Struct containing all the Infos, it could find
 *
 */
function determineReturnValue(searchThrough, types, returnDefaultSettings) {
    let curReturnValue = {
        has: returnDefaultSettings.has,
        type: returnDefaultSettings.type,
        description: returnDefaultSettings.description
    };
    let infrontOfBrackets = searchThrough.substring(0, searchThrough.indexOf("(")).trim();
    let behindBrackets = searchThrough.substring(searchThrough.indexOf(")") + 1, searchThrough.indexOf("{")).trim();
    curReturnValue.type = findWordOfArrayInString(infrontOfBrackets, types);
    if (curReturnValue.type.length === 0) {
        curReturnValue.type = findWordOfArrayInString(behindBrackets, types);
        if (curReturnValue.type.length > 0) {
            curReturnValue.has = true;
        }
        else {
            curReturnValue.has = false;
        }
    }
    else {
        curReturnValue.has = true;
    }
    return curReturnValue;
}
exports.determineReturnValue = determineReturnValue;
/**
 * @author Johannes Roß
 * This function returns an ArgumentDescription[]
 *
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: public]
 *
 * @searchThrough {string} a string in which to find arguments
 * @types {string[]} the types which are avaiable
 *
 * @returns {ArgumentDescription[]} All arguments found in this string
 *
 */
function determineArgumentDescription(searchThrough, types) {
    let allArguments = [{
            name: "",
            description: "",
            type: "",
            required: false
        }];
    let stringWithinFunctionBrackets = "";
    if (searchThrough.indexOf("(") > -1 && searchThrough.indexOf(")") > -1) {
        stringWithinFunctionBrackets = searchThrough.substring(searchThrough.indexOf("(") + 1, searchThrough.indexOf(")"));
        let allArgumentsInString = stringWithinFunctionBrackets.split(",");
        let curArrayPosition = 0;
        allArgumentsInString.map((curArgument) => {
            curArgument = curArgument.trim();
            let newArgument = {
                name: "",
                description: "",
                type: findWordOfArrayInString(curArgument, types),
                required: (curArgument.indexOf("required") !== -1)
            };
            let nameStart = newArgument.required ? "required ".length : 0;
            let searchChar = " ";
            if (curArgument.indexOf(":") !== -1) {
                if (curArgument.indexOf("?:") !== -1) {
                    newArgument.required = false;
                    searchChar = "?";
                }
                else {
                    newArgument.required = true;
                    searchChar = ":";
                }
            }
            else if (curArgument.indexOf(searchChar) === -1) {
                searchChar = "DoNotFindThis";
            }
            if (newArgument.type.length > 0 && searchChar === " ") {
                nameStart += newArgument.type.length + 1;
                if (curArgument.indexOf("=", nameStart) !== -1
                    && (curArgument.indexOf(searchChar, nameStart) !== -1 && curArgument.indexOf(searchChar, nameStart) > curArgument.indexOf("=", nameStart))
                    || curArgument.indexOf(searchChar, nameStart) === -1) {
                    searchChar = "=";
                }
            }
            let nameEnd = curArgument.indexOf(searchChar, nameStart);
            if (searchChar === "DoNotFindThis") {
                nameEnd = curArgument.length;
                newArgument.required = true;
            }
            else if (nameEnd === -1) {
                nameEnd = curArgument.length;
            }
            newArgument.name = curArgument.substring(nameStart, nameEnd);
            let searchForDescription = (curArgument.indexOf("'") > -1 || curArgument.indexOf('"') > -1);
            if (searchForDescription) {
                let searchQuates = curArgument.indexOf("'") > 0 ? "'" : '"';
                newArgument.description = curArgument.substring(curArgument.indexOf(searchQuates) + 1, curArgument.indexOf(searchQuates, curArgument.lastIndexOf(searchQuates)));
            }
            if (allArguments.length == curArrayPosition + 1
                && allArguments[curArrayPosition].name.length
                    + allArguments[curArrayPosition].description.length
                    + allArguments[curArrayPosition].type.length === 0
                && allArguments[curArrayPosition].required === false) {
                allArguments[curArrayPosition] = newArgument;
                curArrayPosition++;
            }
            else {
                allArguments.push(newArgument);
            }
        });
    }
    return allArguments;
}
exports.determineArgumentDescription = determineArgumentDescription;
/**
 * @author Johannes Roß
 * This function returns a scope, which it found in the given string
 *
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: private]
 *
 * @searchThrough {string} a string in which to find the scope
 * @arrayWithValues {string[]} the scopes which are avaiable
 *
 * @returns {string} The determined scope or an empty string
 *
 */
function findWordOfArrayInString(searchThrough, arrayWithValues) {
    let determined = "";
    if (arrayWithValues.length > 0) {
        for (let value of arrayWithValues) {
            let found = false;
            let indexOfItem = searchThrough.indexOf(value);
            if (indexOfItem > -1) {
                let frontFound = (indexOfItem === 0);
                if (!frontFound) {
                    if (searchThrough.charAt(indexOfItem - 1) === " "
                        || searchThrough.charAt(indexOfItem - 1) === ":") {
                        frontFound = true;
                    }
                }
                if (frontFound) {
                    let behindFound = (indexOfItem + value.length === searchThrough.length);
                    if (!behindFound) {
                        if (searchThrough.charAt(indexOfItem + value.length) === " "
                            || searchThrough.charAt(indexOfItem + value.length) === "{"
                            || searchThrough.charAt(indexOfItem + value.length) === "[") {
                            behindFound = true;
                        }
                    }
                    found = behindFound && frontFound;
                }
                if (found) {
                    determined = value;
                    break;
                }
            }
        }
    }
    return determined;
}
/**
 * @author Johannes Roß
 * This function returns all UserSet Default Values or the default values
 *
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: public]
 *
 * @searchThrough {string} a string in which to find the type
 *
 * @returns {string} The determined type or the first type in the types[]
 *
 */
function collectUserDefaultValues(userSettingsFileName) {
    let updatedDefaultSettings = defaultvalues_json_1.default;
    if (fs.existsSync(userSettingsFileName)) {
        let fileContent = fs.readFileSync(userSettingsFileName, { encoding: 'UTF8' });
        try {
            updatedDefaultSettings = JSON.parse(fileContent);
        }
        catch (syntax) {
            updatedDefaultSettings = defaultvalues_json_1.default;
            vscode.window.showErrorMessage('Your JSON file seemed to have a Syntax Error in it. If you don\'nt fix it, it will be overwritten. Syntax Error: ' + syntax.message);
        }
    }
    return updatedDefaultSettings;
}
exports.collectUserDefaultValues = collectUserDefaultValues;
/**
 * @author Johannes Roß
 * This function calculates a integervalue of the current position of the selection
 *
 * [section: Value Collecter]
 * [category: Calculation]
 * [scope: private]
 *
 * @currentTexteditor {TextEditor} a Texteditor where a selection is made
 *
 * @returns {number} An integer with a tabcount
 *
 */
function determineTabs(currentTexteditor) {
    let tabCount = 0;
    try {
        let whitespaceCount = currentTexteditor.document.lineAt(currentTexteditor.selection.anchor.line).firstNonWhitespaceCharacterIndex;
        tabCount = Math.round((whitespaceCount / 4));
    }
    catch (error) {
        tabCount = 0;
    }
    return tabCount;
}
exports.determineTabs = determineTabs;
/**
 * @author Johannes Roß
 * This function saves the current settings in a json file
 *
 * [section: Value Collecter]
 * [category: File Management]
 * [scope: public]
 *
 * @userSettingsFileName {string} the name of the file
 * @commentStruct {CommentStructure} the current settings
 *
 * @returns {CommentStructure} The new saved settings
 *
 */
function writeUserSettings(userSettingsFileName, commentStruct) {
    let updatedDefaultSettings = collectUserDefaultValues(userSettingsFileName);
    // Update settings
    if (!updatedDefaultSettings.scopes.includes(commentStruct.scope, 0)) {
        updatedDefaultSettings.scopes.push(commentStruct.scope);
    }
    if (!updatedDefaultSettings.types.includes(commentStruct.type, 0)) {
        updatedDefaultSettings.types.push(commentStruct.type);
    }
    updatedDefaultSettings.author = commentStruct.author;
    updatedDefaultSettings.section = commentStruct.section;
    updatedDefaultSettings.category = commentStruct.category;
    updatedDefaultSettings.functionDescription = commentStruct.functionDescription;
    fs.writeFile(userSettingsFileName, JSON.stringify(updatedDefaultSettings), function (err) {
        if (err) {
            vscode.window.showErrorMessage('Whoops something went wrong: ' + err);
        }
        else {
            vscode.window.showInformationMessage('Settings saved in: ' + userSettingsFileName);
        }
    });
    return commentStruct;
}
exports.writeUserSettings = writeUserSettings;
/**
 * @author Johannes Roß
 * This function reads current settings from a json file if it already exists
 *
 * [section: Value Collecter]
 * [category: File Management]
 * [scope: public]
 *
 * @userSettingsFileName {string} the name of the file
 * @commentStruct {CommentStructure} the current settings
 *
 * @returns {CommentStructure} The new saved settings
 *
 */
function readUserSettings(userSettingsFileName, commentStruct) {
    const fs = require('fs');
    if (fs.existsSync(userSettingsFileName)) {
        let updatedDefaultSettings = collectUserDefaultValues(userSettingsFileName);
        // Update settings
        commentStruct.author = updatedDefaultSettings.author;
        commentStruct.category = updatedDefaultSettings.category;
        commentStruct.functionDescription = updatedDefaultSettings.functionDescription;
        commentStruct.section = updatedDefaultSettings.section;
        commentStruct.return.has = updatedDefaultSettings.returnValue.has;
        commentStruct.return.description = updatedDefaultSettings.returnValue.description;
        commentStruct.return.type = updatedDefaultSettings.returnValue.type;
    }
    return commentStruct;
}
exports.readUserSettings = readUserSettings;
/**
 * @author Johannes Roß
 * This function searches for a Function Description and returns it, if it finds it
 *
 * [section: Value Collecter]
 * [category: Determine]
 * [scope: export]
 *
 * @searchThrough {string} The string containing the description
 * @defaultDescription {string} The description you want to set, if no Description is found
 *
 * @returns {string} The found description or your set value
 */
function determineFunctionDescription(searchThrough, defaultDescription) {
    let functionDescription = defaultDescription;
    if (searchThrough.indexOf(")") > -1 && searchThrough.indexOf("{") > 0) {
        let behindBrackets = searchThrough.substring(searchThrough.indexOf(")") + 1, searchThrough.indexOf("{"));
        let searchChar = "\"";
        let found = false;
        if (behindBrackets.indexOf(searchChar) > -1 && behindBrackets.indexOf(searchChar) != behindBrackets.lastIndexOf(searchChar)) {
            found = true;
        }
        else if (behindBrackets.indexOf("'") > -1 && behindBrackets.indexOf("'") != behindBrackets.lastIndexOf("'")) {
            searchChar = "'";
            found = true;
        }
        if (found) {
            functionDescription = behindBrackets.substring(behindBrackets.indexOf(searchChar) + 1, behindBrackets.lastIndexOf(searchChar));
        }
    }
    return functionDescription;
}
exports.determineFunctionDescription = determineFunctionDescription;
//# sourceMappingURL=valuecollecter.js.map
"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// The module 'vscode' contains the VS Code extensibility API
const vscode = __importStar(require("vscode"));
const fs = __importStar(require("fs"));
const defaultvalues_json_1 = __importDefault(require("./defaultvalues.json"));
const util_1 = require("util");
const valuecollecter_1 = require("./valuecollecter");
const userSettingsFileName = 'ccfusersettings.json';
// this method is called when your extension is activated
function activate(context) {
    let commentStruct = {
        author: defaultvalues_json_1.default.author,
        functionDescription: defaultvalues_json_1.default.functionDescription,
        section: defaultvalues_json_1.default.section,
        category: defaultvalues_json_1.default.category,
        scope: defaultvalues_json_1.default.scopes[0],
        type: defaultvalues_json_1.default.types[0],
        argsOutput: [
            {
                name: "",
                type: "",
                description: "",
                required: false
            }
        ],
        return: {
            has: defaultvalues_json_1.default.returnValue.has,
            type: defaultvalues_json_1.default.returnValue.type,
            description: defaultvalues_json_1.default.returnValue.description
        }
    };
    if (fs.existsSync(userSettingsFileName)) {
        commentStruct = valuecollecter_1.readUserSettings(userSettingsFileName, commentStruct);
    }
    else {
        valuecollecter_1.writeUserSettings(userSettingsFileName, commentStruct);
    }
    // The command has been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json
    let completeComment = vscode.commands.registerCommand('jofuncce.CommentCompleteFunction', () => {
        let feedbackMessage = 'Nothing happened. You probably don\'t have a file open!';
        let activeTextEditor = vscode.window.activeTextEditor;
        if (activeTextEditor !== undefined) {
            try {
                let functionToComment = activeTextEditor.document.lineAt(new vscode.Position(activeTextEditor.selection.start.line, 0)).text;
                let correctSyntax = functionToComment.indexOf("(") >= 0;
                if (correctSyntax) {
                    correctSyntax = functionToComment.indexOf(")") > 0;
                    if (!correctSyntax) {
                        let curLine = activeTextEditor.selection.start;
                        while (!correctSyntax) {
                            curLine = new vscode.Position(curLine.line + 1, 0);
                            if (activeTextEditor.document.lineCount >= curLine.line) {
                                functionToComment += activeTextEditor.document.lineAt(curLine).text;
                                correctSyntax = functionToComment.indexOf(")") > 0;
                            }
                            else {
                                feedbackMessage = "You don't have a ) in your function!";
                                break;
                            }
                        }
                    }
                    correctSyntax = functionToComment.indexOf("{") > 0;
                    if (!correctSyntax) {
                        let curLine = activeTextEditor.selection.start;
                        while (!correctSyntax) {
                            curLine = new vscode.Position(curLine.line + 1, 0);
                            if (activeTextEditor.document.lineCount >= curLine.line) {
                                functionToComment += activeTextEditor.document.lineAt(curLine).text;
                                correctSyntax = functionToComment.indexOf(")") > 0;
                            }
                            else {
                                feedbackMessage = "You don't have a { in your function!";
                                break;
                            }
                        }
                    }
                }
                else {
                    feedbackMessage = "You do not seem to be in the correct line. Please make sure, that you have (!";
                }
                if (correctSyntax) {
                    let workCopy = functionToComment;
                    let userSettings = valuecollecter_1.collectUserDefaultValues(userSettingsFileName);
                    commentStruct = valuecollecter_1.readUserSettings(userSettingsFileName, commentStruct);
                    commentStruct.argsOutput = valuecollecter_1.determineArgumentDescription(workCopy, userSettings.types);
                    commentStruct.scope = valuecollecter_1.determineScope(workCopy, userSettings.scopes);
                    commentStruct.return = valuecollecter_1.determineReturnValue(workCopy, userSettings.types, userSettings.returnValue);
                    commentStruct.functionDescription = valuecollecter_1.determineFunctionDescription(workCopy, commentStruct.functionDescription);
                    activeTextEditor.insertSnippet(new vscode.SnippetString(assambleBlockComment(commentStruct, valuecollecter_1.determineTabs(activeTextEditor))), new vscode.Position(activeTextEditor.selection.anchor.line, 0));
                    feedbackMessage = 'Successully added function comment.';
                }
            }
            catch (error) {
                console.log(error);
                feedbackMessage = 'Whoops! Something went wrong! Please make sure, that your Syntax is correct!';
            }
        }
        // Display a message box to the user
        vscode.window.showInformationMessage(feedbackMessage);
    });
    let changeCurrentSetting = vscode.commands.registerCommand('jofuncce.ChangeCommentSettings', () => {
        vscode.window.showInputBox({ prompt: "Please enter the Type of Change you want to make." }).then((typeOfChange) => {
            if (util_1.isUndefined(typeOfChange) || typeOfChange.length === 0) {
                vscode.window.showErrorMessage('Please enter after the command a type of change: values: add, update\n' +
                    'a setting, and a newValue');
                return;
            }
            vscode.window.showInputBox({ prompt: "Please enter the setting you want to change." }).then((setting) => {
                if (util_1.isUndefined(setting) || setting.length === 0) {
                    vscode.window.showErrorMessage('Please enter after the command a type of change: values: add, update\n' +
                        'a setting, and a newValue');
                    return;
                }
                vscode.window.showInputBox({ prompt: "Please enter the new value for your change." }).then((newValue) => {
                    if (util_1.isUndefined(newValue) || newValue.length === 0) {
                        vscode.window.showErrorMessage('Please enter after the command a type of change: values: add, update\n' +
                            'a setting, and a newValue');
                        return;
                    }
                    commentStruct = changeSetting(typeOfChange, setting, newValue, commentStruct);
                });
            });
        });
    });
    context.subscriptions.push(completeComment);
    context.subscriptions.push(changeCurrentSetting);
}
exports.activate = activate;
/**
 * @author Johannes Roß
 * This function changes one setting. If wanted it can also save the default.
 *
 * [section: Main Extention]
 * [category: Saving]
 * [scope: private]
 *
 * @typeOfChange {string} the type of change you want to make. Possible values are: "add", "update"
 * @setting {string} the name of the setting you want to change.
 * @newValue {string} the new value you want to change the setting to.
 * @commentStruct {CommentStructure} the current settings
 * @saveDefault {boolean} is the value you are changing a default value, that should be changed.
 *
 * @returns {CommentStructure} The new saved settings
 *
 */
function changeSetting(typeOfChange, setting, newValue, commentStruct) {
    let madeChange = false;
    switch (typeOfChange) {
        case "add":
            madeChange = true;
            switch (setting) {
                case "type":
                case "types":
                    commentStruct.type = newValue;
                    break;
                case "scope":
                case "scopes":
                    commentStruct.scope = newValue;
                    break;
                default:
                    vscode.window.showErrorMessage('You cannot add that setting.');
                    madeChange = false;
                    break;
            }
            break;
        case "up":
        case "update":
            madeChange = true;
            switch (setting) {
                case "author":
                    commentStruct.author = newValue;
                    break;
                case "category":
                    commentStruct.category = newValue;
                    break;
                case "section":
                    commentStruct.section = newValue;
                    break;
                case "functionDescription":
                    commentStruct.functionDescription = newValue;
                    break;
                case "scope":
                    commentStruct.scope = newValue;
                    break;
                case "type":
                    commentStruct.type = newValue;
                default:
                    vscode.window.showErrorMessage('There is no such setting.');
                    madeChange = false;
                    break;
            }
            break;
        default:
            vscode.window.showErrorMessage('That change is not possible.');
            madeChange = false;
            break;
    }
    if (madeChange) {
        valuecollecter_1.writeUserSettings(userSettingsFileName, commentStruct);
    }
    return commentStruct;
}
/**
 * @author Johannes Roß
 * This function creates a String, which is marked up.
 *
 * [section: Main Extention]
 * [category: Assamble]
 * [scope: private]
 *
 * @collectedInfo {CommentStructure} struct containing all needed Infos
 * @timesInlined {number} times the function is tabbed (4 spaces = 1 Tab)
 *
 * @returns {string} A Comment containing the info given
 *
 */
function assambleBlockComment(collectedInfo, timesInlined) {
    let assambledComment = "";
    timesInlined = timesInlined > 0 ? timesInlined : 0;
    assambledComment += "\t".repeat(timesInlined) + "/**\n";
    assambledComment += "\t".repeat(timesInlined) + " * @author " + collectedInfo.author + "\n";
    assambledComment += "\t".repeat(timesInlined) + " * " + collectedInfo.functionDescription + "\n";
    assambledComment += "\t".repeat(timesInlined) + " * \n";
    assambledComment += "\t".repeat(timesInlined) + " * [section: " + collectedInfo.section + "]\n";
    assambledComment += "\t".repeat(timesInlined) + " * [category: " + collectedInfo.category + "]\n";
    assambledComment += "\t".repeat(timesInlined) + " * [scope: " + collectedInfo.scope + "]\n";
    assambledComment += "\t".repeat(timesInlined) + " * \n";
    collectedInfo.argsOutput.forEach((arg) => {
        if (arg.name.length > 0) {
            assambledComment += "\t".repeat(timesInlined) + " * @" + arg.name + " " + (arg.required ? "!" : "") + "{" + arg.type + "} " + arg.description + "\n";
        }
    });
    if (collectedInfo.return.has) {
        assambledComment += "\t".repeat(timesInlined) + " * \n";
        assambledComment += "\t".repeat(timesInlined) + " * @returns {" + collectedInfo.return.type + "} " + collectedInfo.return.description + "\n";
    }
    assambledComment += "\t".repeat(timesInlined) + " */\n";
    return assambledComment;
}
// this method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map